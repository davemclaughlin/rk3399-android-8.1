#!/bin/bash

PRODUCT="nanopc_t4"
VARIANT="userdebug"

source build/envsetup.sh >/dev/null
lunch ${PRODUCT}-${VARIANT}

